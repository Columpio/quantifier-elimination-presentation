import os, string
from typing import List, Optional, Tuple


class Presentations:
    MyPresentation = "wrapper.pdf"
    QuantifierElimination = "pres-smt2017-ajr.pdf"
    TheorySpecific = "pres-iowa2017-part2.pdf"

    TheorySpecificOut = "generated_theory_specific.pdf"
    TheoryIndependentOut = "generated_theory_independent.pdf"
    SourcesOut = "generated_sources.pdf"
    OutPdf = "generated_out.pdf"


def build(presentations: List[Tuple[str, Optional[Tuple[int, int]]]], out: str, compress: bool = False):
    def get_range(rng):
        return "%d-%d" % rng if rng else ""

    let_names = dict(zip({a for a, _ in presentations}, string.ascii_uppercase))
    names = [letter+"="+name for name, letter in let_names.items()]

    ranges = [let_names[name]+get_range(rng) for name, rng in presentations]

    command = "pdftk {names} cat {ranges} output {out}".format(out=out, names=' '.join(names), ranges=' '.join(ranges))

    print(command)
    print("Combining...")
    os.system(command)
    if compress:
        print("Compressing...")
        os.system("convert -compress Zip -density 150x150 {out} {out}".format(out=out))

    print("Done. Output:", out)


TheoryIndependent = [
    (Presentations.MyPresentation, (1, 2)),                 # Intro
    (Presentations.QuantifierElimination, (15, 27)),        # DPLL(T)
    (Presentations.QuantifierElimination, (30, 44)),        # E-matching
    (Presentations.QuantifierElimination, (46, 74)),        # 46+: E-matching challenges; 60+: Conflict-based
    (Presentations.QuantifierElimination, (95, 96)),        # Conflict-based: performance
    # (Presentations.QuantifierElimination, (97, 100)),       # Conflict-based: challenges
    (Presentations.QuantifierElimination, (101, 126)),      # Model-based + impact + challenges
    (Presentations.QuantifierElimination, (127, 131)),      # All together
    (Presentations.QuantifierElimination, (135, 178)),      # Counterexample
    (Presentations.QuantifierElimination, (214, 214)),      # Overview
]
build(TheoryIndependent, Presentations.TheoryIndependentOut)

TheorySpecific = [
    (Presentations.MyPresentation, (3, 5)),
    (Presentations.TheorySpecific, (24, 30)),               # Congruence closure
    (Presentations.TheorySpecific, (42, 46)),               # Bit-vectors
]
build(TheorySpecific, Presentations.TheorySpecificOut)

Sources = [
    (Presentations.MyPresentation, (6, 9))                  # Sources
]
build(Sources, Presentations.SourcesOut)


if True:  # Combine
    slides = [Presentations.TheoryIndependentOut, Presentations.TheorySpecificOut, Presentations.SourcesOut]
    build([(sl, None) for sl in slides], Presentations.OutPdf)
